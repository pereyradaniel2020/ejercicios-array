using System;

namespace ejercicios_array
{
    class Program7
    {
        static void Main(string[] args)
        {
                       //Crear un programa que defina un array de 5 elementos de tipo float que representen las alturas de 5 personas.
            //Obtener el promedio de las mismas. Contar cuántas personas son más altas que el promedio y cuántas más bajas.
            
           //array
           
        float total;
         int alto = 0, bajo = 0;

         float[] altura = {3f,4f,6.4f,5.9f,5.6f};

         total = (altura[0] + altura[1] + altura [2] + altura[3] + altura[4]);
         total /= altura.Length;

         Console.WriteLine("El promedio de las 5 alturas es de {0}: " + total);

         for (int i = 0; i < 5; i++)
         {
             if (altura[i] > total)
             {
             Console.WriteLine("La altura de la persona " + i + " es mayor al promedio");
             alto++ ;
             }
             else
              if (altura[i] < total)
             {
             Console.WriteLine("La altura de la persona " + i + " es menor al promedio");
             bajo++ ;
             }
         }

         //Resultados
         Console.WriteLine("-----------------En total---------------");
         Console.WriteLine("Hay "+ alto + " personas mas altas que el promedio.");
        Console.WriteLine("Hay "+ bajo + " personas mas baja que el promedio.");

          
 
        }
    }
}
