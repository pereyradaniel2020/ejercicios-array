using System;

namespace ejercicios_array
{
    class Program3
    {
        static void Main(string[] args)
        {
          ///Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos.

           //array
      
        int[] numeros = new int[10];
           string[] Contador = {"primer","segundo","tercer","cuarto","quinto","sexto","setimo","octavo","noveno","decimo"};

          // pedir los numeros 
          for (int i = 0; i < numeros.Length; i++)
          {
              Console.WriteLine("Introduzca el " + Contador[i]+" numero");
              numeros[i] = int.Parse(Console.ReadLine());
          }

          /// buscar el numero mayor y presentarlo 
           Array.Sort(numeros);
           Array.Reverse(numeros);
           Console.WriteLine(" El numero mayor de todos los introducidos es el : " + numeros[0]);
  
        }
    }
}
