using System;

namespace ejercicios_array
{
    class Program5
    {          struct datosimagen
        {
            public string nombre;
            public int altura;
            public int ancho;
            public double tamaño;
        }
        
        static void Main(string[] args)
        {
            
            datosimagen[] aImagen = new datosimagen[700];
            int Cont_ficha = 0, menu, i;  
            int contador = 0, contador2 = 0;        
            string buscarnombre;  
            
            do
            {
                Console.Clear();
                Console.WriteLine("-----------------MENU DE OPCIONES-----------------");
                Console.WriteLine("1. - Agregar una ficha nueva.");
                Console.WriteLine("2. - Ver todas las fichas.");
                Console.WriteLine("3. - Buscar la ficha por nombre.");
                Console.WriteLine("4. - Salir del programa.");

                Console.Write("Elija una opcion del menu: ");
                menu = int.Parse(Console.ReadLine());

                switch (menu)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("\n" + "-----------------Agregar una ficha nueva-----------------\n");

                        if (Cont_ficha <= aImagen.Length)
                        {
                                Console.Write("Cual el nombre de la imagen: ");
                                aImagen[contador].nombre = Console.ReadLine();

                                Console.Write("Cual es la altura de la imagen en pixeles: ");
                                aImagen[contador].altura = int.Parse(Console.ReadLine());

                                Console.Write("Cual es el ancho de la imagen en pixeles: ");
                                aImagen[contador].ancho = int.Parse(Console.ReadLine());

                                Console.Write("Cual es el tamaño de la imagen en KB: ");
                                aImagen[contador].tamaño = double.Parse(Console.ReadLine());

                                contador += 1;
                                Console.Write("Precione cualquier tecla..."); 
                                Console.ReadKey();
                        } else
                        {
                            Console.WriteLine("ERROR: REGISTRO DE FICHERO LLENO (700/700)");
                        }
                        
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine("-----------------Ver todas las fichas-----------------");
                        contador2 = 0 ;
                        
                        for (i = 0; i < contador; i++)
                        { 
                            contador2 ++;
                            Console.WriteLine("Fichero Numero " + contador2);
                            Console.WriteLine("Nombre imagen: {0}| Anchura imagen: {1} px| Altura imagen: {2} px| Tamaño imagen: {3} KB",
                            aImagen[i].nombre, aImagen[i].altura, aImagen[i].ancho, aImagen[i].tamaño);
                        }

                        Console.Write("Precione cualquier tecla..."); 
                        Console.ReadKey();

                        break;
                    
                    case 3:
                        Console.Clear();
                        Console.WriteLine("\n-----------------Buscar la ficha por nombre-----------------\n");

                        Console.Write("Ingrese el nombre de la imagen que desea buscar: ");
                        buscarnombre = (Console.ReadLine());
                        
                        for (i = 0; i < contador; i++)
                            if (aImagen[i].nombre == buscarnombre)
                                Console.WriteLine("\n" + "Fichero Encontrado\n" + "Nombre imagen: {0}| Anchura imagen: {1} px| Altura imagen: {2} px| Tamaño imagen: {3} KB\n",
                                aImagen[i].nombre, aImagen[i].altura, aImagen[i].ancho, aImagen[i].tamaño);
                            

                        Console.Write("Precione cualquier tecla..."); 
                        Console.ReadKey();
                        break;

                    case 4:
                        Console.Clear();
                        Console.WriteLine("-----------------Salir del programa-----------------");

                        Console.Write("Precione cualquier tecla..."); 
                        Console.ReadKey();
                        break;
                    
                    default: 
                        Console.WriteLine("\n" + "ERROR: LA OPCION ELEGIDA NO ES VALIDA.");
                        break;

                }
            } while (menu != 4);

        }
    }
}
