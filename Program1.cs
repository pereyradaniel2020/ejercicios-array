using System;

namespace ejercicios_array
{
    class Program1
    {
        static void Main(string[] args)
        {
            //Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float") y luego los muestre en el orden contrario al que se introdujeron.

           //array
           
           float[] numeros = new float[5];
            string[] Contador = {"primer","segundo","tercer","cuarto","quinto"};

            for (int i = 0; i < numeros.Length; i++)
            {
                 Console.WriteLine("Introduzca el valor " + Contador[i]+" numero");
                 numeros[i] = float.Parse(Console.ReadLine());
                
            }
          
        
          Console.WriteLine("Usted introdujo los numeros ");

            foreach (var a in numeros)
           {
               Console.WriteLine(a );
           }

           Console.WriteLine("Que el orden contrario seria");

           Array.Reverse(numeros);
           foreach (var a in numeros)
           {
               Console.WriteLine(a );
           }

        }
    }
}
