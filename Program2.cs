using System;

namespace ejercicios_array
{
    class Program2
    {
        static void Main(string[] args)
        {
            //Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto), pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.

           //array
           int a= 0;
            int[] numeros = {0,31,28,31,30,31,30,31,31,30,31,30,31};
           string[] meses={"Mes","Enero","Febrero","Marzo","Abril","Mayo","junio","Julio","Agosto","septiembre","Octumbre","Noviembre","Diciembre"};
          

          Console.WriteLine("De los meses : ");
          for (int i = 1; i < meses.Length; i++)
          {
              Console.WriteLine(i + " = " + meses[i]);
          }

           Console.WriteLine("Cuatos dias tienes el mes de :");
           a = int.Parse(Console.ReadLine());

           Console.WriteLine("El mes de "+ meses[a] + " tienes " + numeros[a] + " dias");

        }
    }
}
